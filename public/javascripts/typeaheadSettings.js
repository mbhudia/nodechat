//Implementation of typeahead.js
//Typeahead courtesy of @bassjobsen (https://github.com/bassjobsen/Bootstrap-3-Typeahead)

$('#toUser').typeahead({
    source: function(query, process) {
        $.get("/findUsers/"+ query, function(data) {
            if (data) {
                return process(data);
            }
        });
    },
    minLength: 3,
    displayText: function(item) {
        return item.firstName + " " + item.lastName;
    },
    afterSelect: function(item) {
        cvm.addUserToChat(item);
        $('#toUser').val("");
        $('#enterMessage').focus();
    }
});
